Tinder Full Stack Coding Challenge
===================================
Link :http://52.34.88.220/

Time: Approximately 3-4 Hours

Tech: Angular, Hammer

Features:
Stack of cards
Population of data from json 
Matching Screen
Swipe gesture recognition

Interactivity
Enabled dragging the cards
Right click drag slightly: Yes
Right click drag completely :Card disappears and matching screen
Left click drag slightly: No
Right click drag completely :Card disappears

Testing:
Tested on chrome developer tools for iphone 6

Changes:
1. Make the Panning smooth
2. Use python for backend
3. Make it responsive
4. Match the style to the actual tinder style
5. Use sass instead of css
6. Create a proper development environment using bower for dependencies, gulp etc
7. Write test cases
8. Handle error conditions


Other:
1. Create new hammer directives for panning and linking to the card on an event instead of having them linked directly in the current angular directive.
2. Position the elements better