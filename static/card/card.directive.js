angular
	.module('tinderApp')
	.directive('tdCard',tdCard);

function tdCard(){
	var directive={
			restrict: 'EA',
			scope: {data:'=', onRemove:"&"},
			
			templateUrl: '/static/card/card.html',
			link:link,
			replace: true
	};
	return directive;
}
function link(scope, element, attrs){
	var y_pos=scope.data.index*-3;
	element.css({'transform':'translateY('+y_pos+'px)'});
	var mc = new Hammer.Manager(element[0]);
    mc.add(new Hammer.Pan({ threshold: 0, pointers: 0 }));
    mc.on("pan", function(event){
                    var x= (event.deltaX );
                    var y= (event.deltaY );
		             if(x>100){
		            	 scope.$apply(function(){
		            		 scope.data.nope=false;
		            		 scope.data.like=true;
		            		  });
		            	 
		             }
		             else if(x<-100 ){
		            	 scope.$apply(function(){
		            		 scope.data.like=false;
		            		 scope.data.nope=true;
		            		  });
		            	 
		             }
		             
		             
		             else{
		            	 scope.$apply(function(){
		            	 scope.data.like=false;
		        		 scope.data.nope=false;
		            	 });
		             }
		             
            element.css({'transform':'translate('+x+"px,"+y+'px)'});
    });
    mc.on("panend", function(event){
    	if(event.isFinal) {
    		  var x= (event.deltaX )
    		
    		 
    		 window.setTimeout(function(){
    			  if(x>300 ){
        			  scope.$apply(function(){
        				  scope.onRemove({choice:"yes"})});
        		  }
    			  if(x<-300 ){
        			  scope.$apply(function(){
        				  scope.onRemove({choice:"no"})});
        		  }
    			  else{
    			 scope.$apply(function(){
	            	 scope.data.like=false;
	        		 scope.data.nope=false;
	            	 });
    			  }
    			 element.css({'transform':'translate('+0+"px,"+y_pos+'px)'});
               }, 300);    	        
    		
    	}
       
});
	
}
	