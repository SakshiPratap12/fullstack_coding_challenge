angular
	.module('tinderApp')
	.controller('CardStackController',CardStackController);	

function CardStackController(CardStackService) {
    var vm = this;
    vm.getCards=getCards;
    vm.onRemove=onRemove;
    vm.hideMatch=hideMatch;
    vm.getCards();
    
    function getCards()
    {    
    	var promise = CardStackService.getTitles();
	    promise.then(
	       function(payload) { 
	           vm.cards = payload.cards;
	       },
	       function(errorPayload) {
	           $log.error('failure loading posts', errorPayload);
	       });
    }
    function onRemove(index, choice){
    	if(choice==="yes"){
    		vm.match=vm.cards[index];
        	vm.match.show=true;
    	}
    	vm.cards.splice(index,1);
    	
    }
    
    function hideMatch(){
    	vm.match.show=false;
    }
    
    
  
}
