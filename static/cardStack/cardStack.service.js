angular
	.module('tinderApp')
    .service('CardStackService',CardStackService)

function CardStackService($q,$http){
    this.getTitles = getTitles;
    
    function getTitles(){
    	 var deferred = $q.defer();
         $http.get('/static/data/data.json')
           .success(function(data) { 
              deferred.resolve({
                 cards: data});
           }).error(function(msg, code) {
              deferred.reject(msg);
              $log.error(msg, code);
           });
         return deferred.promise;
           }
    }
